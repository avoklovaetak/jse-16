package ru.volkova.tm.api.service;

import ru.volkova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
