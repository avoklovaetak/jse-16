package ru.volkova.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showSystemInfo();

    void exit();

}
