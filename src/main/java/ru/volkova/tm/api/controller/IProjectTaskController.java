package ru.volkova.tm.api.controller;

public interface IProjectTaskController {

    void findAllTasksByProjectId();

    void bindTaskByProjectId();

    void unbindTaskByProjectId();

    void removeProjectById();

}
