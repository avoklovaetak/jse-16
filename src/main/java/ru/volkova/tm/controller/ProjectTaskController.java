package ru.volkova.tm.controller;

import ru.volkova.tm.api.controller.IProjectTaskController;
import ru.volkova.tm.api.service.IProjectTaskService;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.Project;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void findAllTasksByProjectId() {
        System.out.println("[TASK LIST OF PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTasksByProjectId(projectId);
        int index = 1;
        for (final Task task: tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

    @Override
    public void bindTaskByProjectId() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskByProjectId(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void unbindTaskByProjectId() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskByProjectId(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT AND ALL TASKS CASCADE]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}
