package ru.volkova.tm.controller;

import ru.volkova.tm.api.controller.ICommandController;
import ru.volkova.tm.api.service.ICommandService;
import ru.volkova.tm.model.Command;
import ru.volkova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ekaterina Volkova");
        System.out.println("E-MAIL: avoklovaetak@yandex.ru");
    }

    @Override
    public void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showCommands(){
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments(){
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void showHelp(){
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    @Override
    public void showSystemInfo(){
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.format(memoryTotal));
        final long usedMemory = memoryTotal - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

    @Override
    public void exit(){
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
